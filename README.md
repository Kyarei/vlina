## VLiNA

Vocaloid Lyrics in Non-Naturalistic Artlangs.

Translations of lyrics from songs originally sung by a vocal synthesis program into non-naturalistic artistic languages.

### Index

| \# | Title | Original Title | Language |
|----|-------|----------------|----------|
| 0x00 | nasrantel črêca | 深海少女 | ŋarâþ crîþ v7 |

### Licence

This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
